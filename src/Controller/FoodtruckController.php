<?php

namespace App\Controller;

use App\Entity\User;

use App\Entity\Ville;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class FoodtruckController extends Controller
{
    /**
     * @Route("/foodtruck/inscription", name="foodtruckRegistre")
     */
        public function registration(Request $request, UserPasswordEncoderInterface $passwordEncoder, ObjectManager $manager)
        {
            $user = new User();
            $form = $this->createForm(UserType::class, $user );
            $form->handleRequest($request);
           
            if($form->isSubmitted() && $form->isValid()) {
                $password = $passwordEncoder->encodePassword($user, $user->getPassword());
                $user->setPassword($password);
               ;
               

              /*   $nom_entreprise = $request($user->getNomEntreprise());
                $user->setNomEntreprise($nom_entreprise); */
                $user->setRoles('ROLE_FOODTRUCK');
                $manager->persist($user);
    
                $manager->flush();
                return $this->redirectToRoute('login');
    
            }
    
            
            return $this->render('foodtruck/index.html.twig', [
                'form_foodtruck' => $form->createView(),
            ]);
        }
        /**
         * @Route("/foodtruck/listes-foodtrucks", name="foodtrucks")
         */
        public function getAll(UserRepository $repo){
            $foodtrucks = $repo->findAll();

            return $this->render('foodtruck/foodtrucks.html.twig',[
                'foodtrucks' => $foodtrucks,
            ]);
        }
        /**
         * @Route("/foodtruck/{id}", name="foodtruck_show")
         */
        public function show(User $foodtruck) {
           
            return $this->render('foodtruck/show.html.twig',[
                'user' => $foodtruck
            ]);
        }
    }


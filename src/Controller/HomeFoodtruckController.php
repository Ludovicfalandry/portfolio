<?php

namespace App\Controller;

use App\Entity\User;


use App\Entity\Ville;
use App\Form\UserType;
use App\Form\ClientType;

use App\Repository\UserRepository;
use App\Repository\VilleRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeFoodtruckController extends Controller
{
    /**
     * 
     * @Route("/foodtruck", name="home_foodtruck")
     * 
     * 
     */
    public function index(ObjectManager $manager, Request $request, VilleRepository $repo, UserRepository $userRepo)
    {
       

        return $this->render('home_foodtruck/accueil.html.twig', [
            
        ]);
    }
    /**
     * 
     * @Route("/search", name="search")
     * 
     * 
     */
    public function search(ObjectManager $manager, Request $request, VilleRepository $repo, UserRepository $userRepo)
    {
        if($request->query->get('ville')){
            $search = $request->query->get('ville');
        /* dump($search); */
       
            $resultat = $repo->findBy([
                'nom' => $search 
                ]);
               
                 /* dump($resultat); */
               
                foreach ($resultat as $key=>$value) {
                    
                    $idUser[] = $value->getId();
                     dump($idUser );
                     $foods = $userRepo->findByVille(
                     $idUser);
                 } 
                 if (empty($foods) ){
                    return $this->render('foodtruck/search.html.twig', [
                        'search' => 'null'

                    ]); 
                 }
                 dump($foods );
                
                 /* dump($idUse); */
             
                
          
                 return $this->render('foodtruck/search.html.twig', [
                        'search' => $foods

                    ]); 
        }
    } 

        /**
        * 
        * @Route("/foodtruck/profil/{id}", name="foodtruck_profil")
        */
        public function profil(User $user,Request $request) {

            $form = $this->createForm(ClientType::class, $user );
            $form->handleRequest($request);
            return $this->render('home_foodtruck/profil.html.twig',[
                'form_foodtruck' => $form->createView(),
                'user' => $user
            ]);
        }

       
    }
   /*  public function jquery(){
 
       
        $bdd = $this->getDoctrine()->getRepository('VillesRepository');
        $log = $_POST['birds'];
        
         
        $requete = $bdd->prepare('SELECT ville_nom ,ville_code_postal FROM villes WHERE ville_nom LIKE :log LIMIT 0,10'); // j'effectue ma requête SQL grâce au mot-clé LIKE
        $requete->execute(array('log' => $log."%"));
         
        $array = array(); // on créé le tableau
         
        while($donnee = $requete->fetch()) // on effectue une boucle pour obtenir les données
        {
            array_push($array, $donnee['ville_nom'] . ' '.$donnee['ville_code_postal']); // et on ajoute celles-ci à notre tableau
        }
    //    header('Content-Type: application/json');
        echo json_encode($array); // il n'y a plus qu'à convertir en JSON
        
    
    
       } */
   


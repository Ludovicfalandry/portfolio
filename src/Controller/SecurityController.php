<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{

    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authentification){
        $error = $authentification->getLastAuthenticationError();
        $lastUsername = $authentification->getLastUsername();
        
        if(!is_null($error)){
            $error = "<span class='form_error'>Identifiants incorrects !</span> ";
        }else
        {
            "";
        }
        
        return $this->render('home_foodtruck/login.html.twig', array(
            '_username' => $lastUsername,
            'error'         => $error,
        ));
    }
     /**
     * @Route("/logout", name="logout")
     */
    public function logout(){}
    
    
}

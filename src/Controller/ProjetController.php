<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProjetController extends Controller
{
    /**
     * @Route("/realisations", name="realisations")
     */
    public function projets()
    {
        return $this->render('projet/index.html.twig');
    }

    /**
     * @Route("/netflox" , name="netflox")
     */
    public function netflox() {

        return $this->render('projet/netflox.html.twig');
    }
}

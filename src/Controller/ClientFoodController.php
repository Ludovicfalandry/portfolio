<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class ClientFoodController extends Controller
{
    /**
     * @Route("/foodtruck/client/foodtruck", name="client_food")
     */
    public function registration(Request $request, UserPasswordEncoderInterface $passwordEncoder, ObjectManager $manager)
    {
        $user = new User;
      
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
          
            $user->setRoles('ROLE_CLIENT');
            
            $manager->persist($user);

            $manager->flush();
            return $this->redirectToRoute('login');

        }


        return $this->render('client_food/index.html.twig', [
            'form_client' => $form->createView(),
        ]);
    }
}

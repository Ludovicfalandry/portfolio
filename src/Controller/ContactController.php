<?php

namespace App\Controller;

use App\Form\ContactType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index(Request $request, \Swift_Mailer $mailer)
    {
        $form = $this->createForm(ContactType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $contactFormData = $form->getData();

            $message = (new \Swift_Message('Mail envoyé'))
                ->setFrom($contactFormData['from'])
                ->setTo('keke481@gmail.com')
                ->setBody(
                    $contactFormData['message'],
                    'text/plain'
                )
            ;
            dump($message);
            $mailer->send($message);

            $this->addFlash('success', 'Message envoyé!');


            return $this->redirectToRoute('contact');
           
        }
        return $this->render('contact/index.html.twig',[
            'our_form' => $form->createView(),
        ]);
    }
}

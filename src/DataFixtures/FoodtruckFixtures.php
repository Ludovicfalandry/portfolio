<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Ville;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class FoodtruckFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');
        for($i = 1; $i <= 10;$i++){
            $foodtruck = new User();
            $ville = new Ville();
            $foodtruck ->setUsername($faker->firstName())
                       ->setPassword($faker->password())
                       ->setEmail($faker->email())
                       ->setNomEntreprise($faker->company())
                       ->setTel($faker->e164PhoneNumber())
                       ->setRoles('ROLE_FOODTRUCK')
                       ->setVille($ville->setNom($faker->city()));
                     

                       $manager->persist($foodtruck);

        }

        $manager->flush();
    }
}

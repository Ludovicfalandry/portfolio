<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(
 * fields ={"email"},
 * message="Email que vous avez rentré est déja utislisé !"
 * )
 *
 * 
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(min="8", minMessage ="Minimum 8 caracteres !")
     */
    private $password;

    /**
    *@Assert\EqualTo(propertyPath="password", message="Vous n'avez pas tapé le même mot de passe !")
    */
    public $confirm_password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_entreprise;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tel;

    /**
     * @ORM\Column(type="array")
     */
    private $roles ;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ville", cascade={"persist"})
     */
    private $ville;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Villes", inversedBy="users")
     */
    private $city;

  

    public function getId()
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getNomEntreprise(): ?string
    {
        return $this->nom_entreprise;
    }

    public function setNomEntreprise(string $nom_entreprise): self
    {
        $this->nom_entreprise = $nom_entreprise;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getRoles() {
        if (empty($this->roles)) {
        return ['ROLE_USER'];
        }
        return $this->roles;
    }

    public function setRoles($roles)
    {
        $this->roles[] = $roles;

        return $this;
    
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function eraseCredentials(){}

        public function getSalt(){}
    
        public function getCity(): ?Villes
        {
            return $this->city;
        }
    
        public function setCity(?Villes $city): self
        {
            $this->city = $city;
    
            return $this;
        }

}

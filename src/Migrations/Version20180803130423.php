<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180803130423 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE villes (id INT AUTO_INCREMENT NOT NULL, ville_departement VARCHAR(3) DEFAULT NULL, nom VARCHAR(45) DEFAULT NULL, ville_code_postal VARCHAR(255) DEFAULT NULL, ville_longitude_deg DOUBLE PRECISION DEFAULT NULL, ville_latitude_deg DOUBLE PRECISION DEFAULT NULL, ville_longitude_grd VARCHAR(9) DEFAULT NULL, ville_latitude_grd VARCHAR(8) DEFAULT NULL, ville_latitude_dms VARCHAR(8) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE villegros');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE villegros (id INT AUTO_INCREMENT NOT NULL, ville_departement VARCHAR(3) DEFAULT NULL COLLATE utf8_general_ci, nom VARCHAR(45) DEFAULT NULL COLLATE utf8_general_ci, ville_code_postal VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, ville_longitude_deg DOUBLE PRECISION DEFAULT NULL, ville_latitude_deg DOUBLE PRECISION DEFAULT NULL, ville_longitude_grd VARCHAR(9) DEFAULT NULL COLLATE utf8_general_ci, ville_latitude_grd VARCHAR(8) DEFAULT NULL COLLATE utf8_general_ci, ville_longitude_dms VARCHAR(9) DEFAULT NULL COLLATE utf8_general_ci, ville_latitude_dms VARCHAR(8) DEFAULT NULL COLLATE utf8_general_ci, INDEX ville_departement (ville_departement), INDEX ville_nom (nom), INDEX ville_code_postal (ville_code_postal), INDEX ville_longitude_latitude_deg (ville_longitude_deg, ville_latitude_deg), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE villes');
        $this->addSql('ALTER TABLE user CHANGE roles roles VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}

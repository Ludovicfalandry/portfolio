      
$( function() {
    function log( message ) {
      $( "<div>" ).text( message ).prependTo( "#log" );
      $( "#log" ).scrollTop( 0 );
    }
 
    $( "#searchVille" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "/foodtruck",
          dataType: "json",
          type : "POST",
          data: {
            birds : $('#searchVille').val()
          },
          success: function( data ) {
            response( data );
            console.log(data);
          }
        } );
      },
      minLength: 3,
      select: function( event, ui ) {
        log(  ui.item.value);
      }
    } );
  } );
  /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


